//
//  UITextField+Extensions.swift
//  Exercise Core Data Advanced
//
//  Created by Rahul Rawat on 13/07/21.
//

import UIKit

extension UITextField {
    func isError(baseColor: CGColor = UIColor.gray.cgColor, numberOfShakes shakes: Float = 4, revert: Bool = false) {
        let shake: CABasicAnimation = CABasicAnimation(keyPath: "position")
        shake.duration = 0.07
        shake.repeatCount = shakes
        shake.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y: self.center.y))
        shake.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y))
        self.layer.add(shake, forKey: "position")
    }
}
