//
//  Gender+CoreDataProperties.swift
//  Exercise Core Data Advanced
//
//  Created by Rahul Rawat on 14/07/21.
//
//

import Foundation
import CoreData


extension Gender {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Gender> {
        return NSFetchRequest<Gender>(entityName: "Gender")
    }

    @NSManaged public var type: String?

}

extension Gender : Identifiable {

}
