//
//  JsonModel.swift
//  Exercise Core Data Advanced
//
//  Created by Rahul Rawat on 12/07/21.
//

import Foundation

// MARK: - Genders
struct GenderModel: Codable {
    let gender: String
}

typealias Genders = [GenderModel]

// MARK: - Family
struct FamilyModel: Codable {
    let family: String
    let people: [PersonModel]
}

// MARK: - Person
struct PersonModel: Codable {
    let name: String
    let age: Int
    let gender: String
}

typealias Families = [FamilyModel]
