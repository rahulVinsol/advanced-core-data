//
//  Person+CoreDataProperties.swift
//  Exercise Core Data Advanced
//
//  Created by Rahul Rawat on 14/07/21.
//
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person")
    }

    @NSManaged public var age: Int64
    @NSManaged public var name: String?
    @NSManaged public var personId: UUID?
    @NSManaged public var family: Family?
    @NSManaged public var gender: Gender?

}

extension Person : Identifiable {

}
