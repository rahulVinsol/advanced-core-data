//
//  StatsModel.swift
//  Exercise Core Data Advanced
//
//  Created by Rahul Rawat on 14/07/21.
//

import Foundation

class AgeStat: Hashable {
    let sum: Int64
    let count: Int64
    @objc let family: Family
    @objc let gender: Gender
    
    init(sum: Int64, count: Int64, family: Family, gender: Gender) {
        self.sum = sum
        self.count = count
        self.family = family
        self.gender = gender
    }
    
    var averageAge: String { String(format: "%.2f", Float64(sum) / Float64(count)) }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(family.familyId)
        hasher.combine(gender.type)
    }
    
    static func ==(lhs: AgeStat, rhs: AgeStat) -> Bool {
        lhs.family.familyId == rhs.family.familyId && lhs.gender.type == rhs.gender.type
    }
}
