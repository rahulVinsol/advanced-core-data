//
//  TableViewCell.swift
//  Exercise Core Data Advanced
//
//  Created by Rahul Rawat on 12/07/21.
//

import UIKit

class PersonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!

    func bindView(person: Person) {
        nameLabel.text = person.name
        ageLabel.text = "\(person.age)"
        genderLabel.text = person.gender!.type
    }
}
