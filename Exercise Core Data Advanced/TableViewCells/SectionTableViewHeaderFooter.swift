//
//  SectionTableViewCell.swift
//  Exercise Core Data Advanced
//
//  Created by Rahul Rawat on 13/07/21.
//

import UIKit

class SectionTableViewHeaderFooter: UITableViewHeaderFooterView {
    
    //MARK: - Properties
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    private var sectionLabel: UILabel!
    
    //MARK: - Init
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        sectionLabel = UILabel()
        sectionLabel.font = UIFont.systemFont(ofSize: 17.0)
        sectionLabel.numberOfLines = 1
        sectionLabel.sizeToFit()
        
        contentView.layoutMargins = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        
        contentView.addSubview(sectionLabel)
        
        let margins = contentView.layoutMarginsGuide
        sectionLabel.translatesAutoresizingMaskIntoConstraints = false
        sectionLabel.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
        sectionLabel.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
        sectionLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        sectionLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
        
        contentView.backgroundColor = .gray
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Configure
    func bindView(sectionName: String) {
        sectionLabel.text = sectionName
    }
}
