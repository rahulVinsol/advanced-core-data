//
//  FamilyTableViewCell.swift
//  Exercise Core Data Advanced
//
//  Created by Rahul Rawat on 14/07/21.
//

import UIKit

class StatsTableViewCell: UITableViewCell {
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!

    func bindView(result: AgeStat) {
        ageLabel.text = result.averageAge
        genderLabel.text = result.gender.type
    }
}
