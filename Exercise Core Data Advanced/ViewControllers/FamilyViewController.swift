//
//  FamilyViewController.swift
//  Exercise Core Data Advanced
//
//  Created by Rahul Rawat on 14/07/21.
//

import UIKit
import CoreData

protocol FamilyDataSourceDelegate {
    func delete(objectsAt indexes: [IndexPath]?)
}

class FamilyDataSource: UITableViewDiffableDataSource<String, Family> {
    var delegate: FamilyDataSourceDelegate?
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            delegate?.delete(objectsAt: [indexPath])
        }
    }
}
typealias FamilySnapshot = NSDiffableDataSourceSnapshot<String, Family>

class FamilyViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var context: NSManagedObjectContext!
    
    private var deleteButton: UIBarButtonItem!
    private var addButtonItem: UIBarButtonItem!
    
    private lazy var dataSource = makeDataSource()
    
    private lazy var fetchRequestController: NSFetchedResultsController<Family> = {
        let fetchRequest: NSFetchRequest<Family> = Family.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Family.name), ascending: true)]
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        controller.delegate = self
        
        return controller
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = dataSource
        
        addButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        
        deleteButton = UIBarButtonItem(title: "Delete", style: .plain, target: self, action: #selector(deleteTapped))
        deleteButton.tintColor = .red
        
        navigationItem.rightBarButtonItems = [editButtonItem, addButtonItem]
        
        do {
            try fetchRequestController.performFetch()
            applySnapshot()
        } catch {
            
        }
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        navigationItem.leftBarButtonItem = editing ? deleteButton : nil
        deleteButton.isEnabled = false
        
        addButtonItem.isEnabled = !editing
        tableView.setEditing(editing, animated: animated)
        
        super.setEditing(editing, animated: animated)
    }
    
    @objc func addTapped(_ sender: UIButton) {
        showAddFamilyVC(familyToUpdate: nil)
    }
    
    private func makeDataSource() -> FamilyDataSource {
        let dataSource = FamilyDataSource(tableView: tableView, cellProvider: { [weak self] (tableView, indexPath, asset) -> UITableViewCell? in
            guard let self = self else { return nil }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") ?? UITableViewCell(style: .default, reuseIdentifier: "cell")
            
            let family = self.fetchRequestController.object(at: indexPath)
            cell.textLabel?.text = family.name
            
            return cell
        })
        dataSource.delegate = self
        
        return dataSource
    }
    
    func applySnapshot(animatingDifferences: Bool = true) {
        var snapshot = FamilySnapshot()
        
        snapshot.appendSections([""])
        snapshot.appendItems(fetchRequestController.fetchedObjects!, toSection: "")
        DispatchQueue.main.async { [weak self] in
            self?.dataSource.apply(snapshot, animatingDifferences: animatingDifferences, completion: {
                if animatingDifferences {
                    self?.tableView.reloadData()
                }
            })
        }
    }
    
    @objc func textChanged(_ tf: UITextField) {
        var resp : UIResponder = tf
        while !(resp is UIAlertController) { resp = resp.next! }
        let alert = resp as! UIAlertController
        (alert.actions[1] as UIAlertAction).isEnabled = (tf.text != "")
    }
    
    private func showAddFamilyVC(familyToUpdate: Family?) {
        let alert = UIAlertController(title: "Family", message: "\(familyToUpdate == nil ? "Add" : "Update") Family here", preferredStyle: .alert)
        
        alert.addTextField { textField in
            textField.placeholder = "Enter Family name"
            textField.text = familyToUpdate?.name
            textField.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Okay", style: .default) {[weak self] _ in
            guard let self = self else { return }
            
            let context = self.context!
            
            let textField = alert.textFields![0] as UITextField
            
            let family = familyToUpdate ?? Family(context: context)
            if familyToUpdate == nil {
                family.familyId = UUID()
            }
            family.name = textField.text
            
            do {
                try context.save()
            } catch {
                
            }
        })
        (alert.actions[1] as UIAlertAction).isEnabled = false
        present(alert, animated: true, completion: nil)
    }
}

extension FamilyViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        .delete
    }
    
    @objc func deleteTapped(_ sender: UIButton) {
        let sortedIndexes = tableView.indexPathsForSelectedRows?.sorted { $0.row > $1.row }
        delete(objectsAt: sortedIndexes)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isEditing {
            changeDeleteButtonTitle()
        } else {
            showAddFamilyVC(familyToUpdate: fetchRequestController.object(at: indexPath))
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        changeDeleteButtonTitle()
    }
    
    private func changeDeleteButtonTitle() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            let selectedRowsCount = self.tableView.indexPathsForSelectedRows?.count ?? 0
            
            self.deleteButton.title = "Delete" + (selectedRowsCount > 0 ? " (\(selectedRowsCount))" : "")
            
            self.deleteButton.isEnabled = selectedRowsCount > 0
        }
    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        return sourceIndexPath.section != proposedDestinationIndexPath.section ? proposedDestinationIndexPath : sourceIndexPath
    }
}

extension FamilyViewController: NSFetchedResultsControllerDelegate {
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        applySnapshot(animatingDifferences: true)
    }
}

extension FamilyViewController: FamilyDataSourceDelegate {
    func delete(objectsAt indexes: [IndexPath]?) {
        if let selectedRows = indexes {
            var index = 0
            
            var uuidsToDelete = [UUID]()
            
            while index < selectedRows.count {
                let indexPath = selectedRows[index]
                
                tableView.deselectRow(at: indexPath, animated: false)
                
                let family = fetchRequestController.object(at: indexPath)
                uuidsToDelete.append(family.familyId!)
                
                index += 1
            }
            
            if index > 0 {
                do {
                    let predicate = NSPredicate(format: "self.familyId IN %@", uuidsToDelete)
                    let fetchRequest: NSFetchRequest<NSFetchRequestResult> = Family.fetchRequest()
                    fetchRequest.predicate = predicate
                    
                    let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                    batchDeleteRequest.resultType = .resultTypeObjectIDs
                    
                    let result = try context.execute(batchDeleteRequest) as! NSBatchDeleteResult
                    
                    let changes: [AnyHashable: Any] = [NSDeletedObjectsKey: result.result as! [NSManagedObjectID]]
                    NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [context])
                    
                    try context.save()
                } catch {
                    
                }
            }
            changeDeleteButtonTitle()
        }
    }
}
