//
//  PersonAddUpdateViewController.swift
//  Exercise Core Data Advanced
//
//  Created by Rahul Rawat on 13/07/21.
//

import UIKit
import CoreData
import DropDown

class PersonAddUpdateViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var familyTextField: UITextField!
    @IBOutlet weak var createButton: UIButton!
    
    var context: NSManagedObjectContext!
    
    var personSelected: Person?
    
    private var allGenders: [Gender]?
    private var genderDropDown: DropDown?
    private var selectedGenderIndex = -1
    
    private var allFamilies: [Family]?
    private var familyDropDown: DropDown?
    private var selectedFamilyIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchGenders()
        
        if let person = personSelected {
            nameTextField.text = person.name
            ageTextField.text = "\(person.age)"
            genderTextField.text = person.gender?.type
            familyTextField.text = person.family?.name
            
            selectedGenderIndex = allGenders!.firstIndex(of: person.gender!)!
            selectedFamilyIndex = allFamilies!.firstIndex(of: person.family!)!
            createButton.setTitle("Update", for: .normal)
        }
        
        nameTextField.delegate = self
        genderTextField.delegate = self
        ageTextField.delegate = self
        familyTextField.delegate = self
        
        createButton.layer.cornerRadius = 15
    }
    
    private func fetchGenders() {
        do {
            let genderFetchRequest: NSFetchRequest<Gender> = Gender.fetchRequest()
            genderFetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Gender.type), ascending: true)]
            
            allGenders = try context.fetch(genderFetchRequest)
            
            let genderDropDown = DropDown()
            genderDropDown.dataSource = allGenders!.map { $0.type! }
            
            genderDropDown.anchorView = genderTextField
            genderDropDown.selectionAction = { [weak self] index, title in
                self?.selectedGenderIndex = index
                self?.genderTextField.text = title
            }
            
            self.genderDropDown = genderDropDown
            
            let familyFetchRequest: NSFetchRequest<Family> = Family.fetchRequest()
            familyFetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Family.name), ascending: true)]
            
            allFamilies = try context.fetch(familyFetchRequest)
            
            let familyDropDown = DropDown()
            familyDropDown.dataSource = allFamilies!.map { $0.name! }
            familyDropDown.dataSource.append("Add New Family")
            
            familyDropDown.anchorView = familyTextField
            familyDropDown.selectionAction = { [weak self] index, title in
                guard let self = self else { return }
                
                let count = (self.familyDropDown?.dataSource.count ?? 0) - 1
                
                if index < count {
                    self.selectedFamilyIndex = index
                    self.familyTextField.text = title
                } else {
                    self.showAddFamilyVC()
                }
            }
            
            self.familyDropDown = familyDropDown
        } catch {
            
        }
    }
    
    @objc func textChanged(_ tf: UITextField) {
        var resp : UIResponder = tf
        while !(resp is UIAlertController) { resp = resp.next! }
        let alert = resp as! UIAlertController
        (alert.actions[1] as UIAlertAction).isEnabled = (tf.text != "")
    }
    
    private func showAddFamilyVC() {
        let alert = UIAlertController(title: "Add Family", message: "Add new Family here", preferredStyle: .alert)
        
        alert.addTextField { textField in
            textField.placeholder = "Enter Family name"
            textField.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Okay", style: .default) {[weak self] _ in
            guard let self = self else { return }
            
            let context = self.context!
            
            let textField = alert.textFields![0] as UITextField
            
            let family = Family(context: context)
            family.name = textField.text
            family.familyId = UUID()
            
            do {
                try context.save()
                
                let index = max((self.familyDropDown?.dataSource.count ?? 0) - 1, 0)
                
                self.familyDropDown?.dataSource.insert(family.name!, at: index)
                self.allFamilies?.append(family)
                self.selectedFamilyIndex = index
                
                self.familyTextField.text = family.name
            } catch {
                
            }
        })
        (alert.actions[1] as UIAlertAction).isEnabled = false
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func createButtonClicked(_ sender: UIButton) {
        var errorOccured = false
        
        if nameTextField.text?.isEmpty != false {
            nameTextField.isError()
            errorOccured = true
        }
        
        if ageTextField.text?.isEmpty != false {
            ageTextField.isError()
            errorOccured = true
        }
        
        if familyTextField.text?.isEmpty != false {
            familyTextField.isError()
            errorOccured = true
        }
        
        if genderTextField.text?.isEmpty != false {
            genderTextField.isError()
            errorOccured = true
        }
        
        if errorOccured {
            return
        }
        
        let person = personSelected ?? Person(context: context)
        
        if personSelected == nil {
            person.personId = UUID()
        }
        
        person.name = nameTextField.text!
        person.age = Int64(ageTextField.text!)!
        person.gender = allGenders![selectedGenderIndex]
        person.family = allFamilies![selectedFamilyIndex]
        
        do {
            try context.save()
            navigationController?.popViewController(animated: true)
        } catch {
            
        }
    }
}

extension PersonAddUpdateViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case genderTextField:
            genderDropDown?.show()
            return false
        case familyTextField:
            familyDropDown?.show()
            return false
        default:
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField != ageTextField {
            return true
        }
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        return
            string.rangeOfCharacter(from: invalidCharacters) == nil
            && (textField.text?.isEmpty == false || string != "0")
            && Decimal(string: (textField.text! as NSString).replacingCharacters(in: range, with: string)) ?? 0 <= 100
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            ageTextField.becomeFirstResponder()
        case ageTextField:
            ageTextField.resignFirstResponder()
        default:
            break
        }
        return true
    }
}
