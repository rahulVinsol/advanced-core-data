//
//  StatsViewController.swift
//  Exercise Core Data Advanced
//
//  Created by Rahul Rawat on 14/07/21.
//

import UIKit
import CoreData

fileprivate struct StatConstants {
    static let SumColumnName = "sum"
    static let CountColumnName = "count"
    static let FamilyColumnName = "family"
    static let GenderColumnName = "gender"
}

class StatsViewController: UIViewController {
    
    @IBOutlet weak var familyTableView: UITableView!
    
    private lazy var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    private var dataSource: Dictionary<String, [AgeStat]> = [:]
    private var sections: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getAverageAgesByFamilyAndGender()
        
        familyTableView.delegate = self
        familyTableView.dataSource = self
    }
    
    private func getAverageAgesByFamilyAndGender() {
        let keypathExp = NSExpression(forKeyPath: #keyPath(Person.age))
        
        let sumExpression = NSExpression(forFunction: "sum:", arguments: [keypathExp])

        let sumDesc = NSExpressionDescription()
        sumDesc.expression = sumExpression
        sumDesc.name = StatConstants.SumColumnName
        sumDesc.expressionResultType = .integer64AttributeType
        
        let countExpression = NSExpression(forFunction: "count:", arguments: [keypathExp])
        
        let countDesc = NSExpressionDescription()
        countDesc.expression = countExpression
        countDesc.name = StatConstants.CountColumnName
        countDesc.expressionResultType = .integer64AttributeType
        
        let request: NSFetchRequest<NSDictionary> = NSFetchRequest(entityName: String(describing: Person.self))
        request.sortDescriptors = [NSSortDescriptor(key: #keyPath(AgeStat.family.name), ascending: true), NSSortDescriptor(key: #keyPath(AgeStat.gender.type), ascending: true)]
        request.returnsObjectsAsFaults = false
        request.propertiesToGroupBy = [StatConstants.FamilyColumnName, StatConstants.GenderColumnName]
        request.propertiesToFetch = [StatConstants.FamilyColumnName, StatConstants.GenderColumnName, countDesc, sumDesc]
        request.resultType = .dictionaryResultType
        
        do {
            dataSource = Dictionary(grouping: try context.fetch(request).map { result in
                AgeStat(
                    sum: result[StatConstants.SumColumnName] as! Int64,
                    count: result[StatConstants.CountColumnName] as! Int64,
                    family: context.object(with: result[StatConstants.FamilyColumnName] as! NSManagedObjectID) as! Family,
                    gender: context.object(with: result[StatConstants.GenderColumnName] as! NSManagedObjectID) as! Gender
                )
            }) {
                $0.family.name!
            }
            sections = dataSource.keys.sorted(by: <)
            
            familyTableView.reloadData()
        } catch {
            
        }
    }
}

extension StatsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource[sections[section]]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: StatsTableViewCell.self), for: indexPath) as! StatsTableViewCell
        
        let stat = dataSource[sections[indexPath.section]]![indexPath.row]
        cell.bindView(result: stat)
        
        return cell
    }
}

extension StatsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        sections[section]
    }
}
