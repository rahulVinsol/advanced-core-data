//
//  ViewController.swift
//  Exercise Core Data Advanced
//
//  Created by Rahul Rawat on 12/07/21.
//

import UIKit
import CoreData

protocol DataSourceDelegate {
    func delete(objectsAt indexes: [IndexPath]?)
}

class DataSource: UITableViewDiffableDataSource<String, Person> {
    var context: NSManagedObjectContext!
    var delegate: DataSourceDelegate?
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            delegate?.delete(objectsAt: [indexPath])
        }
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let sourceItem = (itemIdentifier(for: sourceIndexPath)! as Person)
        let destinationItem = itemIdentifier(for: IndexPath(row: 0, section: destinationIndexPath.section))!
        
        sourceItem.family = destinationItem.family
        
        do {
            try context.save()
        } catch {
            
        }
    }
}

typealias Snapshot = NSDiffableDataSourceSnapshot<String, Person>

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private lazy var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    private var deleteButton: UIBarButtonItem!
    private var addButtonItem: UIBarButtonItem!
    private var listFamilyButtonItem: UIBarButtonItem!
    private var statsButtonItem: UIBarButtonItem!
    
    private var selectedPerson: Person?
    
    private lazy var dataSource = makeDataSource()
    
    private lazy var fetchRequestController: NSFetchedResultsController<Person> = {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Person.family.name), ascending: true), NSSortDescriptor(key: #keyPath(Person.name), ascending: true)]
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: #keyPath(Person.family.name), cacheName: nil)
        controller.delegate = self
        
        return controller
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = dataSource
        
        tableView.register(SectionTableViewHeaderFooter.self, forHeaderFooterViewReuseIdentifier: SectionTableViewHeaderFooter.reuseIdentifier)
        
        addButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        
        listFamilyButtonItem = UIBarButtonItem(title: "Family", style: .plain, target: self, action: #selector(listFamilyTapped))
        
        deleteButton = UIBarButtonItem(title: "Delete", style: .plain, target: self, action: #selector(deleteTapped))
        deleteButton.tintColor = .red
        
        statsButtonItem = UIBarButtonItem(title: "Stats", style: .plain, target: self, action: #selector(statsTapped))
        
        navigationItem.rightBarButtonItems = [editButtonItem, addButtonItem]
        navigationItem.leftBarButtonItems = [listFamilyButtonItem, statsButtonItem]
        
        preloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        do {
            try fetchRequestController.performFetch()
            applySnapshot(animatingDifferences: false)
        } catch {
            
        }
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        if editing {
            navigationItem.leftBarButtonItems?.removeAll()
            navigationItem.leftBarButtonItem = deleteButton
        } else {
            navigationItem.leftBarButtonItems?.removeAll()
            navigationItem.leftBarButtonItems = [listFamilyButtonItem, statsButtonItem]
        }
        
        deleteButton.isEnabled = false
        
        addButtonItem.isEnabled = !editing
        tableView.setEditing(editing, animated: animated)
        
        super.setEditing(editing, animated: animated)
    }
    
    private func preloadData() {
        let genderFetchRequest: NSFetchRequest<Gender> = Gender.fetchRequest()
        genderFetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Gender.type), ascending: true)]
        do {
            let allGenders = try context.fetch(genderFetchRequest)
            
            if allGenders.isEmpty {
                if let filepath = Bundle.main.path(forResource: "json", ofType: "json") {
                    let contents = try String(contentsOfFile: filepath)
                    let decoder = JSONDecoder()
                    
                    let genders = try decoder.decode(Genders.self, from: Data(contents.utf8))
                    
                    for genderModel in genders {
                        let gender = Gender(context: context)
                        gender.type = genderModel.gender
                    }
                    
                    try context.save()
                }
            }
        } catch {
        
        }
    }
    
    /*
     do {
         let families = try decoder.decode(Families.self, from: Data(contents.utf8))
         for familyModel in families {
             let family = Family(context: context)
             family.familyId = UUID()
             family.name = familyModel.family
             
             for personModel in familyModel.people {
                 let person = Person(context: context)
                 person.personId = UUID()
                 person.name = personModel.name
                 person.age = Int64(personModel.age)
                 
                 family.addToPeople(person)
             }
         }
         
         try context.save()
     } catch {
         
     }
    */
    
    @objc func addTapped(_ sender: UIButton) {
        selectedPerson = nil
        performSegue(withIdentifier: String(describing: PersonAddUpdateViewController.self), sender: self)
    }
    
    @objc func listFamilyTapped(_ sender: UIButton) {
        performSegue(withIdentifier: String(describing: FamilyViewController.self), sender: self)
    }
    
    @objc func statsTapped(_ sender: UIButton) {
        performSegue(withIdentifier: String(describing: StatsViewController.self), sender: self)
    }
    
    private func makeDataSource() -> DataSource {
        let dataSource = DataSource(tableView: tableView, cellProvider: { [weak self] (tableView, indexPath, asset) -> UITableViewCell? in
            guard let self = self else { return nil }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PersonTableViewCell.self), for: indexPath) as? PersonTableViewCell
            
            let person = self.fetchRequestController.object(at: indexPath)
            cell?.bindView(person: person)
            
            return cell
        })
        dataSource.context = context
        dataSource.delegate = self
        
        return dataSource
    }
    
    func applySnapshot(animatingDifferences: Bool = true) {
        var snapshot = Snapshot()
        
        if let sections = fetchRequestController.sections {
            for section in sections {
                let sectionName = section.name
                
                snapshot.appendSections([sectionName])
                
                if let people = section.objects as? [Person] {
                    snapshot.appendItems(people, toSection: sectionName)
                }
            }
        }
        DispatchQueue.main.async { [weak self] in
            self?.dataSource.apply(snapshot, animatingDifferences: animatingDifferences, completion: {
                if animatingDifferences {
                    self?.tableView.reloadData()
                }
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case String(describing: PersonAddUpdateViewController.self):
            let destination = segue.destination as! PersonAddUpdateViewController
            destination.personSelected = selectedPerson
            destination.context = context
            
        case String(describing: FamilyViewController.self):
            (segue.destination as! FamilyViewController).context = context
        default:
            break
        }
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: SectionTableViewHeaderFooter.self)) as? SectionTableViewHeaderFooter
        
        if let familyName = self.fetchRequestController.sections?[section].name {
            cell?.bindView(sectionName: familyName)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        .delete
    }
    
    @objc func deleteTapped(_ sender: UIButton) {
        let sortedIndexes = tableView.indexPathsForSelectedRows?.sorted { $0.row > $1.row }
        delete(objectsAt: sortedIndexes)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isEditing {
            changeDeleteButtonTitle()
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
            selectedPerson = fetchRequestController.object(at: indexPath)
            performSegue(withIdentifier: String(describing: PersonAddUpdateViewController.self), sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        changeDeleteButtonTitle()
    }
    
    private func changeDeleteButtonTitle() {
        let selectedRowsCount = tableView.indexPathsForSelectedRows?.count ?? 0
        
        deleteButton.title = "Delete" + (selectedRowsCount > 0 ? " (\(selectedRowsCount))" : "")
        
        deleteButton.isEnabled = selectedRowsCount > 0
    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        return sourceIndexPath.section != proposedDestinationIndexPath.section ? proposedDestinationIndexPath : sourceIndexPath
    }
}

extension ViewController: NSFetchedResultsControllerDelegate {
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        applySnapshot(animatingDifferences: type != NSFetchedResultsChangeType.move)
    }
}

extension ViewController: DataSourceDelegate {
    func delete(objectsAt indexes: [IndexPath]?) {
        if let selectedRows = indexes {
            var index = 0
            
            var uuidsToDelete = [UUID]()
            
            while index < selectedRows.count {
                let indexPath = selectedRows[index]
                
                tableView.deselectRow(at: indexPath, animated: false)
                
                let person = fetchRequestController.object(at: indexPath)
                uuidsToDelete.append(person.personId!)
                
                index += 1
            }
            
            if index > 0 {
                do {
                    let predicate = NSPredicate(format: "self.personId IN %@", uuidsToDelete)
                    let fetchRequest: NSFetchRequest<NSFetchRequestResult> = Person.fetchRequest()
                    fetchRequest.predicate = predicate
                    
                    let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                    batchDeleteRequest.resultType = .resultTypeObjectIDs
                    
                    let result = try context.execute(batchDeleteRequest) as! NSBatchDeleteResult
                    
                    let changes: [AnyHashable: Any] = [NSDeletedObjectsKey: result.result as! [NSManagedObjectID]]
                    NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [context])
                } catch {
                    
                }
            }
            changeDeleteButtonTitle()
        }
    }
}
